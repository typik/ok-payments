#!/usr/bin/env python
#-*- coding: utf-8 -*-

import csv

csv_file= open('data.csv', encoding='utf-8')
data = csv.DictReader(csv_file,delimiter=';')



def checkData(data):
    res=[]
    countries = ['Иран', 'Iran', 'КНДР', 'Корейская Народно-Демократическая Республика', 'Северная Корея', 'KPPP', 'Democratic People\'s Republic of Korea', 'North Korea']
    acc_num = ['40803', '40813', '40820', '40824', '40826', '426']
    pay_details_insur = ['страх', 'возмещение']
    pay_details_owner = ['купли-продажи', 'квартир', 'участк', 'гараж', 'недвижимост', 'дом', 'здани', 'помещени', 'строени', 'яхт', 'судн', 'корабл', 'самолет']
    acc_num_nko = ['40503', '40603', '40703', '416', '419', '422']
    for p in data:
        if int(p['Сумма (руб.)'].replace(".","")) >= 60000000 and p['БИК плательщика'] == '044583119' and '20202' in p['Расч. счет получателя'][:5] and (len(p['ИНН плательщика']) == 10 or len(p['ИНН плательщика']) == 12):
            p.update({'Код операции': '1001'})
            res.append(p)
        if int(p['Сумма (руб.)'].replace(".","")) >= 60000000 and '20202' in p['Расч. счет плательщика'][:5] and p['БИК получателя'] == '044583119' and (len(p['ИНН получателя']) == 10 or len(p['ИНН получателя']) == 12):
            p.update({'Код операции': '1002'})
            res.append(p)
        if int(p['Сумма (руб.)'].replace(".","")) >= 60000000 and any(x in p['Расч. счет плательщика'][:5] for x in acc_num) and p['БИК получателя'] == '044583119' and any(y in p['Адрес плательщика'] for y in countries):
            p.update({'Код операции': '3001'})
            res.append(p)
        if int(p['Сумма (руб.)'].replace(".","")) >= 60000000 and any(x in p['Расч. счет получателя'][:5] for x in acc_num) and p['БИК получателя'] == '044583119' and any(y in p['Адрес получателя'] for y in countries):
            p.update({'Код операции': '3001'})
            res.append(p)
        if int(p['Сумма (руб.)'].replace(".","")) >= 60000000 and p['БИК получателя'] == '044583119' and (len(p['БИК плательщика']) != 9 or 'IR' in p['БИК плательщика'][4:6] or 'KP' in p['БИК плательщика'][4:6]) and (any(y in p['Адрес банка плательщика'] for y in countries)):
            p.update({'Код операции': '3001'})
            res.append(p)
        if int(p['Сумма (руб.)'].replace(".","")) >= 60000000 and p['БИК получателя'] == '044583119' and ('IR' in p['Банк плательщика'] or 'KP' in p['Банк плательщика']) and (any(y in p['Адрес банка плательщика'] for y in countries)):
            p.update({'Код операции': '3001'})
            res.append(p)
        if int(p['Сумма (руб.)'].replace(".","")) >= 60000000 and p['БИК получателя'] == '044583119' and '423' in p['Расч. счет получателя'][:3] and (p['ИНН плательщика'] != p['ИНН получателя'] or p['Наименование плательщика'] != p['Наименование получателя']) and ('предъявител' in p['Назначение платежа']):
            p.update({'Код операции': '4001'})
            res.append(p)
        if int(p['Сумма (руб.)'].replace(".","")) >= 60000000 and p['БИК получателя'] == '044583119' and ('423' in p['Расч. счет получателя'][:3] or '40817' in p['Расч. счет получателя'][:5]) and any(z in p['Назначение платежа'] for z in pay_details_insur):
            p.update({'Код операции': '5002'})
            res.append(p)
        if int(p['Сумма (руб.)'].replace(".","")) >= 300000000 and any(v in p['Назначение платежа'] for v in pay_details_owner):
            p.update({'Код операции': '8001'})
            res.append(p)
        if int(p['Сумма (руб.)'].replace(".","")) >= 10000000 and p['БИК плательщика'] == '044583119' and any(t in p['Расч. счет плательщика'][:5] for t in acc_num_nko):
            p.update({'Код операции': '9002'})
            res.append(p)
    csv_columns = ['\ufeffНомер операции','Дата','БИК плательщика','Банк плательщика','Адрес банка плательщика','ИНН плательщика','Наименование плательщика','Адрес плательщика','Расч. счет плательщика','Сумма (руб.)','БИК получателя','Банк получателя','Адрес банка получателя','ИНН получателя','Наименование получателя','Адрес получателя','Расч. счет получателя','Назначение платежа','Код операции']
    with open('out_res.csv', 'w', encoding='utf-8') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
        writer.writeheader()
        for row in res:
            writer.writerow(row)

checkData(data)