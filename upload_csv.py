#!/usr/bin/env python
#-*- coding: utf-8 -*-
import os, csv
from payment_order import PaymentOrder

DATA_FILE = "data.csv"

def openFile(file):
    with open(file, 'r',encoding='utf-8-sig') as myfile:
        return myfile.read()

def openFile2(file):
    k = []
    with open(file, 'r',encoding='utf-8') as myfile:
        reader = csv.DictReader(myfile, delimiter=';')
        for row in reader:
            k.append(row)
    return k

def getParam(row):
    order=PaymentOrder(summ=row['Сумма'],
        order_number=row['\ufeffНомер операции'],
        order_date=row['Дата'],
        payer_bik=row['БИК плательщика'],
        payer_bank_name=row['Банк плательщика'],
        payer_bank_address=row['Адрес банка плательщика'],
        payer_inn=row['ИНН плательщика'],
        payer_name=row['Наименование плательщика'],
        payer_address=row['Адрес плательщика'],
        payer_account_number=row['Расч. счет плательщика'],
        beneficiary_bik=row['БИК получателя'],
        beneficiary_bank_name=row['Банк получателя'],
        beneficiary_bank_address=row['Адрес банка получателя'],
        beneficiary_inn=row['ИНН получателя'],
        beneficiary_name=row['Наименование получателя'],
        beneficiary_address=row['Адрес получателя'],
        beneficiary_account_number=row['Расч. счет получателя'],
        payment_details=row['Назначение платежа'])
    return order

if __name__ == '__main__':
    if not os.path.exists(DATA_FILE):
        raise Exception( DATA_FILE + " doesn't exist in working directory" )

    get_data = openFile2(DATA_FILE)
    for row in get_data:
        order_l = getParam(row)
        if PaymentOrder.hitRule1001(order_l) is not None:
            print(str(PaymentOrder.hitRule1001(order_l)) + '  Номер операции: ' + str(order_l.order_number))
        if PaymentOrder.hitRule1002(order_l) is not None:
            print('Номер операции: ' + str(order_l.order_number) + '  '  + str(PaymentOrder.hitRule1002(order_l)))
        if PaymentOrder.hitRule3001_1(order_l) is not None:
            print('Номер операции: ' + str(order_l.order_number) + '  '  + str(PaymentOrder.hitRule3001_1(order_l)))