#!/usr/bin/env python
#-*- coding: utf-8 -*-

import unittest
from payment_order import PaymentOrder

class PaymentOrderTestCase(unittest.TestCase):
    def test_gotBigSumm(self):
        self.assertFalse(PaymentOrder.gotBigSumm(summ='5999.00'))
        self.assertTrue(PaymentOrder.gotBigSumm(summ='6000.00'))
        self.assertTrue(PaymentOrder.gotBigSumm(summ='6000.01'))

    def test_gotBeneficiaryTargetBank(self):
        self.assertTrue(PaymentOrder.gotBeneficiaryTargetBank('044583119'))
        self.assertFalse(PaymentOrder.gotBeneficiaryTargetBank('044583111'))

    def test_gotPayerTargetBank(self):
        self.assertTrue(PaymentOrder.gotPayerTargetBank('044583119'))
        self.assertFalse(PaymentOrder.gotPayerTargetBank('044583111'))


if __name__ == '__main__':
    unittest.main()