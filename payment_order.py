#!/usr/bin/env python
#-*- coding: utf-8 -*-

class PaymentOrder:
    target_bank_bik = '044583119'
    target_summ = '600000'

    countries = ['Иран', 'Iran', 'КНДР', 'Корейская Народно-Демократическая Республика', 'Северная Корея', 'KPPP', 'Democratic People\'s Republic of Korea', 'North Korea']
    person_account_numbers = ['40803', '40813', '40820', '40824', '40826', '426']
    payment_details_insurance = ['страх', 'возмещение']
    payment_details_owner = ['купли-продажи', 'квартир', 'участк', 'гараж', 'недвижимост', 'дом', 'здани', 'помещени', 'строени', 'яхт', 'судн', 'корабл', 'самолет']
    account_numbers_nko = ['40503', '40603', '40703', '416', '419', '422']

    def __init__(self, **payment_data):
        self.order_number = payment_data.get('order_number')
        self.order_date = payment_data.get('order_date')
        self.payer_bik = payment_data.get('payer_bik')
        self.payer_bank_name = payment_data.get('payer_bank_name')
        self.payer_bank_address = payment_data.get('payer_bank_address')
        self.payer_inn = payment_data.get('payer_inn')
        self.payer_name = payment_data.get('payer_name')
        self.payer_address = payment_data.get('payer_address')
        self.payer_account_number = payment_data.get('payer_account_number')
        self.summ = payment_data.get('summ')
        self.beneficiary_bik = payment_data.get('beneficiary_bik')
        self.beneficiary_bank_name = payment_data.get('beneficiary_bank_name')
        self.beneficiary_bank_address = payment_data.get('beneficiary_bank_address')
        self.beneficiary_inn = payment_data.get('beneficiary_inn')
        self.beneficiary_name = payment_data.get('beneficiary_name')
        self.beneficiary_address = payment_data.get('beneficiary_address')
        self.beneficiary_account_number = payment_data.get('beneficiary_account_number')
        self.payment_details = payment_data.get('payment_details')


    #hit rules start here
    def hitRule1001(self):
        if self.gotBigSumm() and self.gotBeneficiaryGotCash() and self.gotPayerTargetBank() and self.gotPayerIsCompany():
            return 'Операция подлежит обязательному контролю. Код операции: 1001.'

    def hitRule1002(self):
        if self.gotBigSumm() and self.gotPayerPaidInCash() and self.gotBeneficiaryTargetBank() and self.gotBeneficiaryIsCompany():
            return 'Операция подлежит обязательному контролю. Код операции: 1002.'

    def hitRule3001_1(self):
        if self.gotBigSumm() and self.gotBeneficiaryTargetBank() and self.gotPayerFromIranOrNorthKorea() and self.gotPayerIsPerson():
            return 'Операция подлежит обязательному контролю. Код операции: 3001(1).'
    
    def hitRule3001_2(self):
        if self.gotBigSumm() and self.gotBeneficiaryTargetBank() and self.gotPayerFromIranOrNorthKorea() and self.gotPayerIsPerson():
            return 'Операция подлежит обязательному контролю. Код операции: 3001(1).'



    #helpers start here
    def gotBigSumm(self):
        order_summ=self.summ.replace(',','').replace('.','')
        if order_summ >= self.target_summ:
            return True
        else:
            return False

    def gotBeneficiaryTargetBank(self):
        if self.beneficiary_bik == self.target_bank_bik:
            return True
        else:
            return False

    def gotPayerTargetBank(self):
        if self.payer_bik == self.target_bank_bik:
            return True
        else:
            return False

    def gotPayerFromIranOrNorthKorea(self):
        if self.payer_address in self.countries:
            return True
        else:
            return False

    def gotPayerIsPersonByAccountNumber(self):
        if self.payer_account_number[:5] in self.person_account_numbers:
            return True
        else:
            return False

    def gotPayerIsPersonByInnLength(self):
        if len(self.payer_inn) != 10:
            return True
        else:
            return False

    def gotPayerIsCompany(self):
        if len(self.payer_inn) == 10:
            return True
        else:
            return False

    def gotBeneficiaryIsCompany(self):
        if len(self.beneficiary_inn) == 10:
            return True
        else:
            return False

    def gotPayerPaidInCash(self):
        if '20202' in self.payer_account_number[:5]:
            return True
        else:
            return False

    def gotBeneficiaryGotCash(self):
        if '20202' in self.beneficiary_account_number[:5]:
            return True
        else:
            return False
